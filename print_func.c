/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_func.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdabiel <fdabiel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/12 16:58:31 by fdabiel           #+#    #+#             */
/*   Updated: 2013/12/16 15:02:02 by fdabiel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int				i_putchar(int c)
{
	write(1, &c, 1);
	return (1);
}

int				ui_putnbr(unsigned int n)
{
	size_t		i;
	
	i = ft_strlen(ft_uitoa(n));
	ft_putstr(ft_uitoa(n));
	return (i);
}

int				i_putnbr(int n, int j)
{
	size_t		i;
	
	i = ft_strlen(ft_itoa(n));
	if (j && n >= 0)
		i += i_putchar(' ');
	ft_putstr(ft_itoa(n));
	return (i);
}

int			i_putstr(char const *s)
{
	char	*ptr;

	if (s)
	{
		ptr = (char *) s;
		while (*ptr)
			ptr += 1;
		write(1, s, (ptr - s));
		return (ptr - s);
	}
	else
		write(1, "(null)", 6);
	return (6);
}
