/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdabiel <fdabiel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/12 08:43:04 by fdabiel           #+#    #+#             */
/*   Updated: 2013/12/16 18:10:23 by fdabiel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_printf.h"

int			main(void)
{
	int		ret1;
	int		ret2;
	ft_putendl("Test --");
	ft_putstr("ft_printf : ");
	ret1 = ft_printf("Coucou %--  --d %   d %s %c %u %  %x %X %p %% %Z\n", 666, -42, NULL, 'o', 300000000, 255, 255, &ft_putstr);
	ft_printf(" ret = %d\n", ret1);
	ft_putstr("printf : ");
	ret2 = printf("Coucou %--  --d %   d %s %c %u %  %x %X %p %% %Z\n", 666, -42, NULL, 'o', 300000000, 255, 255, &ft_putstr);
	ft_printf(" ret = %d\n", ret2);
	return (0);
}
