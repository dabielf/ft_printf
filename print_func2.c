/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_func2.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdabiel <fdabiel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/13 15:32:35 by fdabiel           #+#    #+#             */
/*   Updated: 2013/12/15 19:27:00 by fdabiel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int				o_putnbr(unsigned int n)
{
	size_t		i;
	
	i = ft_strlen(ft_oitoa(n));
	ft_putstr(ft_oitoa(n));
	return (i);
}

int				x_putnbr(unsigned int n, int c)
{
	size_t		i;
	
	i = ft_strlen(ft_xitoa(n, c));
	ft_putstr(ft_xitoa(n, c));
	return (i);
}

int				i_putaddr(long unsigned int n)
{
	size_t		i;

	if (n == 0)
		return(i_putstr("0x0"));
	i = 2;
	ft_putstr("0x");
	i += ft_strlen(ft_xitoa(n, 'x'));
	ft_putstr(ft_xitoa(n, 'x'));
	return (i);
}
