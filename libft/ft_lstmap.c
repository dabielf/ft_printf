/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdabiel <fdabiel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/28 12:10:07 by fdabiel           #+#    #+#             */
/*   Updated: 2013/11/28 12:10:09 by fdabiel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list		*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*ret;
	t_list	*tmp;

	if (lst)
	{
		ret = (*f)(ft_lstnew(lst->content, lst->content_size));
		tmp = ret;
	}
	while (lst->next)
	{
		lst = lst->next;
		tmp->next = (*f)(ft_lstnew(lst->content, lst->content_size));
		tmp = tmp->next;
	}
	return (ret);
}
