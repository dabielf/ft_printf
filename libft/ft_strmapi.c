/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdabiel <fdabiel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/25 11:05:19 by fdabiel           #+#    #+#             */
/*   Updated: 2013/11/28 11:12:29 by fdabiel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	size_t	i;
	size_t	j;
	char	*r;

	i = 0;
	j = 0;
	while (s[i])
		i++;
	r = ft_strnew(i);
	if (r && f)
	{
		while (j < i)
		{
			r[j] = (*f)(j, s[j]);
			j++;
		}
	}
	return (r);
}
