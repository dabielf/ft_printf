/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdabiel <fdabiel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/25 16:00:19 by fdabiel           #+#    #+#             */
/*   Updated: 2013/11/25 19:51:44 by fdabiel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char	*str;
	size_t	i;

	i = 0;
	str = (char *) malloc(sizeof(char) * (len + 1));
	if (str)
	{
		while (i < len)
			str[i++] = s[start++];
		str[i] = 0;
		return (str);
	}
	return (NULL);
}
