/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdabiel <fdabiel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/28 12:07:21 by fdabiel           #+#    #+#             */
/*   Updated: 2013/12/10 10:54:44 by fdabiel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list		*ft_lstnew(void const *content, size_t content_size)
{
	t_list	*newlist;

	newlist = (t_list *)malloc(sizeof(t_list));
	if (newlist)
	{
		if (content)
		{
			newlist->content = malloc(content_size);
			ft_memcpy(newlist->content, content, content_size);
			newlist->content_size = content_size;
		}
		else
		{
			newlist->content = NULL;
			newlist->content_size = 0;
		}
		newlist->next = NULL;
	}
	return (newlist);
}
