/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdabiel <fdabiel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/19 15:23:30 by fdabiel           #+#    #+#             */
/*   Updated: 2013/12/10 11:03:42 by fdabiel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		*ft_memset(void *b, int c, size_t len)
{
	size_t	i;

	i = 0;
	if (b != NULL)
	{
		while (i < len)
		{
			*((char*)b + i) = c;
			i += 1;
		}
		return (b);
	}
	return (NULL);
}
