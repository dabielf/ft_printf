/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdabiel <fdabiel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/10 11:28:20 by fdabiel           #+#    #+#             */
/*   Updated: 2013/12/16 17:50:46 by fdabiel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <stdarg.h>
# include <libft.h>

int		ft_printf(const char *format, ...);
int		check_opt(const char *format, va_list *ap, size_t *i, size_t *n);
int		i_putnbr(int n, int j);
int		ui_putnbr(unsigned int n);
int		o_putnbr(unsigned int n);
int		i_putstr(char const *s);
int		i_putchar(int c);
int		x_putnbr(unsigned int n, int c);
int		i_putaddr(long unsigned int n);
int		optlist(char c);
void	do_flag(const char *format, va_list *ap, size_t *i,size_t *n, int *j);
int		is_flag(const char *format, va_list *ap, size_t *i,size_t *n);
void	check_white(const char *format, size_t *i, int *j);
#endif /* !FT_PRINTF_H */
