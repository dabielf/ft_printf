/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_opt.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdabiel <fdabiel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/12 18:34:28 by fdabiel           #+#    #+#             */
/*   Updated: 2013/12/16 17:59:05 by fdabiel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	do_flag(const char *format, va_list *ap, size_t *i,size_t *n, int *j)
{
	if (format[*i + 1] == 'd' || format[*i + 1] == 'i')
		*n += i_putnbr(va_arg(*ap, int), *j);
	if (format[*i + 1] == 'u')
		*n += ui_putnbr(va_arg(*ap, int));
	if (format[*i + 1] == 'o')
		*n += o_putnbr(va_arg(*ap, int));
	if (format[*i + 1] == 's')
		*n += i_putstr(va_arg(*ap, char *));
	if (format[*i + 1] == 'c')
		*n += i_putchar(va_arg(*ap, int));
	if (format[*i + 1] == 'x' || format[*i + 1] == 'X')
		*n += x_putnbr(va_arg(*ap, int), format[*i + 1]);
	if (format[*i + 1] == 'p')
		*n += i_putaddr(va_arg(*ap, long unsigned int));
}

int		optlist(char c)
{
	char	*options;
	int		i;

	options = "diuoscxXp";
	i = 0;
	while (options[i])
	{
		if (options[i] == c)
			return (i);
		i++;
	}
	return (-1);
}

void	check_white(const char *format, size_t *i, int *j)
{
	char	*white;

	white = " -#";
	while (ft_strchr(white, format[*i + 1]))
	{
		if (format[*i + 1] == ' ')
		{
			*i += 1;
			*j += 1;
		}
		if (format[*i + 1] == '#' || format[*i + 1] == '-')
			*i += 1;
	}
}

int		is_flag(const char *format, va_list *ap, size_t *i,size_t *n)
{
	int		j;

	j = 0;
	check_white(format, i, &j);
	if (optlist(format[*i + 1]) >= 0 )
	{
		do_flag(format, ap, i, n, &j);
		*i += 2;
		if (format[*i] == '%')
		{
			is_flag(format, ap, i, n);
		}
		return (1);
	}
	return (0);
}

int		check_opt(const char *format, va_list *ap, size_t *i, size_t *n)
{
	if (!is_flag(format, ap, i, n))
		*i += 1;
	if (format[*i] == 0)
	{
		va_end(*ap);
		return (*n);
	}
	return (0);
}
