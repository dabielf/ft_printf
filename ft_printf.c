/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdabiel <fdabiel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/10 11:24:26 by fdabiel           #+#    #+#             */
/*   Updated: 2013/12/16 17:30:38 by fdabiel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int				ft_printf(const char *format, ...)
{
	size_t		i;
	size_t		n;
	va_list		ap;

	if (!format)
		return (0);
	va_start(ap, format);
	i = 0;
	n = 0;
	while (format[i])
	{
		if (format[i] == '%')
		{
			if (check_opt(format, &ap, &i, &n))
				return (n);
		}
		n += i_putchar(format[i]);
		i++;
	}
	va_end(ap);
	return (n);
}
