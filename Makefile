# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: fdabiel <fdabiel@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/12/12 10:45:58 by fdabiel           #+#    #+#              #
#    Updated: 2013/12/13 16:15:13 by fdabiel          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libftprintf.a
INC = libft/includes/
LIBD = libft/
LIBA = libft/libft.a
CC = gcc
FLAGS = -Wall -Wextra -Werror
FILES = ft_memset \
		ft_bzero \
		ft_memcpy \
		ft_memccpy \
		ft_memmove \
		ft_memchr \
		ft_memcmp \
		ft_strlen \
		ft_strdup \
		ft_strcpy \
		ft_strncpy \
		ft_strcat \
		ft_strncat \
		ft_strlcat \
		ft_strchr \
		ft_strrchr \
		ft_strstr \
		ft_strnstr \
		ft_strcmp \
		ft_strncmp \
		ft_atoi \
		ft_isalpha \
		ft_isdigit \
		ft_isalnum \
		ft_isascii \
		ft_isprint \
		ft_toupper \
		ft_tolower \
		ft_memalloc \
		ft_memdel \
		ft_strnew \
		ft_strdel \
		ft_strclr \
		ft_striter \
		ft_striteri \
		ft_strmap \
		ft_strmapi \
		ft_strequ \
		ft_strnequ \
		ft_strsub \
		ft_strjoin \
		ft_strtrim \
		ft_itoa \
		ft_uitoa \
		ft_oitoa \
		ft_xitoa \
		ft_putchar \
		ft_putnbr \
		ft_putstr \
		ft_putendl \
		ft_putchar_fd \
		ft_putstr_fd \
		ft_putendl_fd \
		ft_putnbr_fd \
		ft_lstnew \
		ft_lstdelone \
		ft_lstdel \
		ft_lstadd \
		ft_lstiter \
		ft_lstmap
LIBPF = ft_printf \
		print_func \
		print_func2 \
		check_opt

all : $(NAME)

$(NAME) :
	@$(CC) $(FLAGS) -c $(addprefix $(LIBD), $(addsuffix .c, $(FILES)))
	@$(CC) $(FLAGS) -c $(addsuffix .c, $(LIBPF)) -I $(LIBD)
	@ar rc $(NAME) $(addsuffix .o, $(LIBPF)) $(addsuffix .o, $(FILES))
	@ranlib $(NAME)

clean :
	@/bin/rm -f $(addsuffix .o, $(FILES))
	@/bin/rm -f $(addsuffix .o, $(LIBPF))

fclean : clean
	/bin/rm -f $(NAME)

re : fclean all

lib : all clean
